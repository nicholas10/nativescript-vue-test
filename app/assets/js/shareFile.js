import {
  ShareFile
} from 'nativescript-share-file';
import * as fs from 'tns-core-modules/file-system';

export class TestClass {

  // shareFile;
  // fileName;
  // documents;
  // path;
  // file;

  constructor() {

    this.fileName = 'text.txt';
    this.documents = fs.knownFolders.documents();
    this.path = fs.path.join(this.documents.path, this.fileName);
    this.file = fs.File.fromPath(this.path);
    this.shareFile = new ShareFile();

    this.shareFile.open({
      path: this.path,
      intentTitle: 'Open text file with:', // optional Android
      rect: { // optional iPad
        x: 110,
        y: 110,
        width: 0,
        height: 0
      },
      options: true, // optional iOS
      animated: true // optional iOS
    });
  }
}